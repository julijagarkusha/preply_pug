const popupShow = (event) => {
  event.preventDefault();
  document.querySelector("body").classList.add("popup--show");
};

export default popupShow;