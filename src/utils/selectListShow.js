const selectListShow = (event) => {
  const target = event.target;
  if(target.classList.contains("select--custom")) {
    const selectElements = document.querySelectorAll(".select__wrapper");
    selectElements.forEach(selectElement => {
      selectElement.classList.remove("select__wrapper--open")
    });
    target.parentNode.classList.add("select__wrapper--open");
  }

};

export default selectListShow;
