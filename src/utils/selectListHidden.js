const selectListHidden = (event) => {
  const target = event.target;
  if(!target.classList.contains("select__option") && !target.classList.contains("select--custom")) {
    const selectElements = document.querySelectorAll(".select__wrapper");
    selectElements.forEach(selectElement => {
      selectElement.classList.remove("select__wrapper--open")
    });
  }
  console.log(target);
};

export default selectListHidden;
