const cardsToggle = (event) => {
  const target = event.target;
  const cardsToggleButtons = document.querySelectorAll(".content__show--custom");
  if(!target.classList.contains("content__show--active")) {
    cardsToggleButtons.forEach(cardsToggleButton => {
      cardsToggleButton.classList.remove("content__show--active");
      target.classList.add("content__show--active")
    });
  }
};

export default cardsToggle;