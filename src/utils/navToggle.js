const addActive = (targetElement) => {
  const navItems = document.querySelectorAll(".nav__item");
  navItems.forEach(navItem => {
    navItem.classList.remove("nav__item--active");
    targetElement.classList.add("nav__item--active")
  });
};

const navToggle = (event) => {
  const target = event.target;
  if(!target.classList.contains("nav__item--active") && target.classList.contains("nav__item")) {
    addActive(target);
  } else if (!target.parentNode.classList.contains("nav__item--active")) {
    addActive(target.parentNode);
  }
};

export default navToggle;