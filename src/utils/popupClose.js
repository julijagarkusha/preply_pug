const popupClose = (event) => {
  const target = event.target;
  const targetParent = target.parentNode;
  if(target.classList.contains("popup--close") || targetParent.classList.contains("popup--close") || target.classList.contains('button--cancel')) {
    document.querySelector("body").classList.remove("popup--show");
    console.log('target', target);
  }
};

export default popupClose;