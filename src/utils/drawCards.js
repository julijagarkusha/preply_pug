const createCard = (wrapper, card, categories) => {
  let categoryCard = categories.find(category => {
    if(card.category === category.id){
      return category;
    }
  });
  const cardElement = document.createElement("article");
  cardElement.innerHTML = sessionStorage.getItem('card');
  wrapper.appendChild(cardElement);
  cardElement.classList.value = "card";
  cardElement.querySelector(".card--checked input").id = card.id;
  cardElement.querySelector(".card--checked label").setAttribute("for", card.id);
  cardElement.querySelector(".card__original").innerHTML = card.original;
  cardElement.querySelector(".card__translation").innerHTML = card.translation;
  if (card.category !== undefined) {
    cardElement.querySelector(".card__category").innerHTML = categoryCard.name;
  } else {
    cardElement.querySelector(".card__category").innerHTML = 'Не обрана';
  }
  cardElement.querySelector(".card__date").innerHTML = card.date;
  cardElement.querySelector(".card__progress").innerHTML = card.progress;
};

const drawCards = (cards, categories, html) => {
  const cardsWrapper = document.querySelector(".cards");
  cardsWrapper.innerHTML = "";
  if(sessionStorage.getItem('card')) {
    cards.forEach(card => {
      createCard(cardsWrapper, card, categories);
    });
  } else {
    fetch(html).then(res => {
      return res.text();
    }).then(data => {
      sessionStorage.setItem('card', data);
      cards.forEach(card => {
        createCard(cardsWrapper, card, categories);
      });
    })
  }
};

export default drawCards;