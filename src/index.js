import "./styles/index.scss";
import cards from "./mocks/carsd";
import categories from "./mocks/categories";
import drawCards from "./utils/drawCards";
import cardsToggle from "./utils/cardsToggle";
import navToggle from "./utils/navToggle";
import popupClose from "./utils/popupClose";
import popupShow from "./utils/popupShow";
import addWord from "./utils/addWord";
import selectListShow from "./utils/selectListShow";
import selectListHidden from "./utils/selectListHidden";

const cardsToggleButtons = document.querySelectorAll(".content__show--custom");
cardsToggleButtons.forEach(cardsToggleButton => {
  cardsToggleButton.addEventListener("click", cardsToggle);
});

const navItems = document.querySelectorAll(".nav__item");
navItems.forEach(navItem => {
  navItem.addEventListener("click", navToggle)
});

const popupShowElements = document.querySelectorAll(".button--add");
popupShowElements.forEach(popupShowElement => {
  popupShowElement.addEventListener("click", popupShow);
});


const popupCloseElement = document.querySelector(".popup--close");
if(popupCloseElement) {
  popupCloseElement.addEventListener("click", popupClose);
}

const selectListElements = document.querySelectorAll(".select__wrapper");
selectListElements.forEach(selectListElement => {
  selectListElement.addEventListener("mouseover", selectListShow)
});

const containerElement = document.querySelector("main");
containerElement.addEventListener("mouseover", selectListHidden);

window.onscroll = () => {
  let scrolled = window.pageYOffset || document.documentElement.scrollTop;
  const headerElement = document.querySelector(".header--fixed");
  if(scrolled > 85) {
    headerElement.classList.add("header--show");
  } else if (scrolled < 85 && headerElement.classList.contains("header--show")) {
    headerElement.classList.remove("header--show");
  }
};

const popupButtons = document.querySelectorAll(".form__actions .button");
popupButtons.forEach(popupButton => {
  if(popupButton.getAttribute('value') === "cancel") {
    popupButton.addEventListener("click", popupClose);
  } else {
    popupButton.addEventListener("click", addWord)
  }
});

drawCards(cards, categories, 'card.html');

