export default [
  {
    id: "word1",
    original: "A bad hair day",
    translation: "День, коли все валиться з рук",
    date: "22.01.2018",
    progress: "100%"
  },
  {
    id: "word2",
    original: "Dog",
    translation: "Собака",
    category: "category1",
    date: "02.01.2018",
    progress: "93%"
  },
  {
    id: "word3",
    original: "A blessing in disguise",
    translation: "Не було б щастя, та нещастя допомогло; щось таке, що спочатку було погане, але потім призвело до хороших результатів",
    category: "category2",
    date: "31.01.2018",
    progress: "92%"
  },
  {
    id: "word4",
    original: "T-shirt",
    translation: "Футболка",
    category: "category3",
    date: "31.01.2018",
    progress: "48%"
  },
  {
    id: "word5",
    original: "Blouse",
    translation: "Кофта",
    category: "category3",
    date: "02.01.2018",
    progress: "33%"
  },
  {
    id: "word6",
    original: "Refrigerator",
    translation: "Холодильник",
    category: "category4",
    date: "22.01.2018",
    progress: "50%"
  },
  {
    id: "word7",
    original: "Kitchen",
    translation: "Кухня",
    category: "category4",
    date: "02.01.2018",
    progress: "67%"
  }
]